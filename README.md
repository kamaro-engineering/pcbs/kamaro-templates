# Kamaro Engineering e.V. Symbols

This repository contains the official Kamaro Engineering e.V. KiCAD templates.

**The libraries in this repository are intended for KiCad version 6.x**

Other KiCad library repositories are located:

* Symbols: https://gitlab.com/kamaro-engineering/pcbs/kamaro-symbols
* Footprints: https://gitlab.com/kamaro-engineering/pcbs/kamaro-footprints
* 3D Models: https://gitlab.com/kamaro-engineering/pcbs/kamaro-packages3d
